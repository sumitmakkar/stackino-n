#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int                   data;
    struct node_structure *next;
}node_struct;

class MyStack
{
private:
    node_struct *stackTop;
    node_struct *minStackTop;
    
    node_struct* createNode(int num)
    {
        node_struct *newNode = new node_struct();
        if(newNode)
        {
            newNode->data = num;
            newNode->next = NULL;
        }
        return newNode;
    }
    
public:
    MyStack()
    {
        stackTop    = NULL;
        minStackTop = NULL;
    }
    
    void pushNode(int num)
    {
        node_struct *newNode = createNode(num);
        if(!stackTop)
        {
            stackTop    = newNode;
            minStackTop = newNode;
        }
        else
        {
            newNode->next = stackTop;
            stackTop      = newNode;
            if(minStackTop->data >= newNode->data)
            {
                node_struct *newMinStackElement = newNode;
                newMinStackElement->next        = minStackTop;
                minStackTop                     = newMinStackElement;
            }
        }
    }
    
    void popNode()
    {
        if(stackTop)
        {
            node_struct *nodeToBeDeleted = stackTop;
            stackTop                     = stackTop->next;
            if(nodeToBeDeleted == minStackTop)
            {
                node_struct *minStackNodeToBeDeleted = minStackTop;
                minStackTop                          = minStackTop->next;
                free(minStackNodeToBeDeleted);
                minStackNodeToBeDeleted              = NULL;
            }
            //free(nodeToBeDeleted);
            nodeToBeDeleted              = NULL;
        }
    }
    
    int getTopElement()
    {
        return stackTop->data;
    }
    
    int getMinElement()
    {
        return minStackTop->data;
    }
    
    void display()
    {
        node_struct *head = stackTop;
        while(head)
        {
            cout<<head->data<<" ";
            head = head->next;
        }
        cout<<endl;
    }
};

int main(int argc, const char * argv[])
{
    MyStack myStack = MyStack();
    vector<int> stackElementsVector = {5,4,2,7,9,10,5,3,15};
    for(int i = 0 ; i < (int)stackElementsVector.size() ; i++)
    {
        myStack.pushNode(stackElementsVector[i]);
    }
    myStack.display();
    myStack.popNode();
    myStack.popNode();
    myStack.popNode();
    myStack.popNode();
    myStack.popNode();
    //    myStack.popNode();
    //    myStack.popNode();
    cout<<myStack.getTopElement()<<endl;
    cout<<myStack.getMinElement()<<endl;
    return 0;
}
